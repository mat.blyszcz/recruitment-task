// This file is written in ES5 since it's not transpiled by Babel.
// Sets Node environment variable
process.env.NODE_ENV = 'test';

// Registers babel for transpiling our code for testing
require('babel-register')();

// Disable webpack-specific features for tests since
// Mocha doesn't know what to do with them.
require.extensions['.css'] = function () {return null;};
require.extensions['.png'] = function () {return null;};
require.extensions['.jpg'] = function () {return null;};