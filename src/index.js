import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import "normalize.css/normalize.css";

import reducers from './reducers';
import ProfileContainer from './containers/ProfileContainer';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <ProfileContainer />
    </Provider>,
    document.getElementById('root')
);