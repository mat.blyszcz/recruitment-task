import React from 'react';
import PropTypes from 'prop-types';
import Comment from './Comment';
import { Scrollbars } from 'react-custom-scrollbars';

const CommentsList = ({comments, showComments}) => {
    let commentsClass = `comments timeline__comments ${showComments ? "": "comments--hide"}`;
    return (
        <Scrollbars className={commentsClass} style={{height: 470}}
                    renderTrackVertical={({ style, ...props }) =>
                        <div className="scrollbar__track-vertical"
                             {...props} style={{ ...style, top: 0, right: 0, height: '100%' }}/>
                    }
                    renderThumbVertical={props =>
                        <div {...props} className="scrollbar__thumb-vertical"/>
                    }
        >
            {comments
                .sort((a, b) =>  b.daysAgo - a.daysAgo )
                .map(comment =>
                    <Comment key={comment._id} comment={comment} />
                )
            }
        </Scrollbars>
    );
};

CommentsList.propTypes = {
  comments: PropTypes.array.isRequired,
  showComments: PropTypes.bool.isRequired
};

export default CommentsList;