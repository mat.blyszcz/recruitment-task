import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Widget from './Widget';
import Modal from './Modal';

class ProfileCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showShareModal: false
        };

        this.toggleShareModal = this.toggleShareModal.bind(this);
    }

    toggleShareModal(value) {
        this.setState({showShareModal: value});
    }

    render() {
        let {details, onLike, onFollow} = this.props;
        const likeBtnClass = `fa fa-heart${details.isLiked ? " profile-card__like-icon--active": "-o"} profile-card__like-icon`;
        const followBtnClass = `button button--${details.isFollowed ? "disabled" : "orange"} profile-card__follow-btn`;

        return (
            <div className="profile-card">
                { this.state.showShareModal && <Modal url={details.profileUrl} onClose={() => this.toggleShareModal(false)} />}
                <i className="fa fa-share-square-o profile-card__share-btn" aria-hidden="true" onClick={() => this.toggleShareModal(true)}></i>
                <div className="profile-card__details">
                    <img className="profile-card__img" src={details.profilePhoto} alt="profile-face" />
                    <div className="profile-card__info">
                        <p className="profile-card__fullname">{details.fullname}
                            <i onClick={onLike} className={likeBtnClass}></i></p>
                        <p className="profile-card__address">{details.address}</p>
                    </div>
                </div>
                <div className="profile-card__summary">
                    <div className="widgets profile-card__widgets">
                        <Widget title={"Likes"} counter={details.likes} />
                        <div className="widgets__separator"></div>
                        <Widget title={"Following"} counter={details.following} />
                        <div className="widgets__separator"></div>
                        <Widget title={"Followers"} counter={details.followers} />
                    </div>
                    <button onClick={onFollow} type="button" className={followBtnClass}>
                        FOLLOW
                    </button>
                </div>
            </div>
        );
    }
}

ProfileCard.propTypes = {
    details: PropTypes.object.isRequired,
    onLike: PropTypes.func.isRequired,
    onFollow: PropTypes.func.isRequired
};

export default ProfileCard;