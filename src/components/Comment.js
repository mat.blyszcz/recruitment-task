import PropTypes from 'prop-types';
import React from 'react';

const Comment = ({comment}) => {
    return (
        <div className="comments__comment">
            <img className="comments__img" src={comment.profilePhoto} alt="profile-face" />
            <div className="comments__details">
                <div className="comments__desc">
                    <p className="comments__author">{comment.fullname}</p>
                    <p className="comments__day">{comment.daysAgo}d</p>
                </div>
                <p className="comments__content">{comment.content}</p>
            </div>
        </div>
    );
};

Comment.propTypes = {
    comment: PropTypes.object.isRequired
};

export default Comment;