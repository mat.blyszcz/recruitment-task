import React from 'react';
import PropTypes from 'prop-types';
import { ModalContainer, ModalDialog } from 'react-modal-dialog';

const Modal = ({onClose, url}) => {
    return (
        <ModalContainer onClose={onClose}>
            {
                <ModalDialog onClose={onClose}>
                    <h1>Share</h1>
                    <p>Share this profile: {url}</p>
                </ModalDialog>
            }
        </ModalContainer>
    );
};

Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired
};

export default Modal;