import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from "../actions/index";
import ProfileCard from "../components/ProfileCard";
import Timeline from '../components/Timeline';

class ProfileContainer extends Component {
    constructor(props) {
        super(props);

        this.onLikeProfile = this.onLikeProfile.bind(this);
        this.onFollowProfile = this.onFollowProfile.bind(this);
    }

    componentDidMount() {
        this.props.fetchProfile();
    }

    onLikeProfile() {
        this.props.profile.details.isLiked ? this.props.unlikeProfile() : this.props.likeProfile();
    }

    onFollowProfile() {
        this.props.profile.details.isFollowed ? this.props.unfollowProfile() : this.props.followProfile();
    }

    render() {
        if(!Object.keys(this.props.profile).length) {
            return (null);
        }
        return (
            <div className="profile-container">
                <ProfileCard
                    details={this.props.profile.details}
                    onLike={this.onLikeProfile}
                    onFollow={this.onFollowProfile}
                />
                <Timeline
                    comments={this.props.profile.comments}
                    onCommentAdd={(comment) => this.props.addComment(comment)}
                />
            </div>
        );
    }
}

ProfileContainer.propTypes = {
    profile: PropTypes.object.isRequired,
    fetchProfile: PropTypes.func.isRequired,
    likeProfile: PropTypes.func.isRequired,
    unlikeProfile: PropTypes.func.isRequired,
    followProfile: PropTypes.func.isRequired,
    unfollowProfile: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        profile: state.profile
    }
}

export default connect(mapStateToProps, actions)(ProfileContainer);