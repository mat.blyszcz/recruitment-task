import ProfileApi from '../api/mockProfileApi';
import * as types from './actionTypes';

function fetchProfileSuccess(profile) {
    return {
        type: types.FETCH_PROFILE,
        profile: profile
    };
}

export function fetchProfile() {
    return dispatch => {
        return ProfileApi.getProfile().then(profile => {
            dispatch(fetchProfileSuccess(profile));
        });
    };
}

function likeProfileSuccess() {
    return {
        type: types.LIKE_PROFILE
    };
}

export function likeProfile() {
    return dispatch => {
        return ProfileApi.likeProfile().then(() => {
            dispatch(likeProfileSuccess());
        });
    };
}

function unlikeProfileSuccess() {
    return {
        type: types.UNLIKE_PROFILE
    };
}

export function unlikeProfile() {
    return dispatch => {
        return ProfileApi.unlikeProfile().then(() => {
            dispatch(unlikeProfileSuccess());
        });
    };
}

function followProfileSuccess() {
    return {
        type: types.FOLLOW_PROFILE
    };
}

export function followProfile() {
    return dispatch => {
        return ProfileApi.followProfile().then(() => {
            dispatch(followProfileSuccess());
        });
    };
}

function unfollowProfileSuccess() {
    return {
        type: types.UNFOLLOW_PROFILE
    };
}

export function unfollowProfile() {
    return dispatch => {
        return ProfileApi.unfollowProfile().then(() => {
            dispatch(unfollowProfileSuccess());
        });
    };
}

function addCommentSuccess(comment) {
    return {
        type: types.ADD_COMMENT,
        comment: comment
    };
}

export function addComment(comment) {
    return dispatch => {
        return ProfileApi.addComment(comment).then(comment => {
            dispatch(addCommentSuccess(comment));
        });
    };
}