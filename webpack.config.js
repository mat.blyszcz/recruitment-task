console.log(__dirname);
module.exports = {
    entry: [
        './src/index.js'
    ],
    output: {
        path: __dirname + '/public/js/',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['react', 'es2015', 'stage-1'],
                            ignore: '/node_modules/'
                        }
                    }
                ]
            },
            {test: /\.css$/, use: [ 'style-loader', 'css-loader' ]},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [ 'file-loader' ]},
            {test: /\.(woff|woff2)$/, use: [ 'file-loader' ]},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [ 'file-loader' ]},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: [ 'file-loader' ]},
            {test: /\.jpg$/, use: [ 'file-loader' ]}
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};